Windows Defender Endpoint for Linux Ansible Role
=========

[Deploy Microsoft Defender for Endpoint for Linux](https://docs.microsoft.com/en-us/windows/security/threat-protection/microsoft-defender-atp/linux-install-with-ansible).
This Repository contains an ansible role to install Microsoft Defender Endpoint for Linux on main OSSes.

This is a modified and (molecule improved) Version of https://github.com/deekayen/ansible-role-mde

If an error occurs during installation, the installer will only report a general failure. The detailed log will be saved to `/var/log/microsoft/mdatp/install.log`.

Requirements
------------

- ansible
- OSes:
  - Ubuntu >= 1804 (LTE versions)
  - Debian >= 10
  - Redhat >= 7
  - Fedora >= 30 (not well tested)

Support for Archlinux/Manjaro will be added later.

Role Variables
--------------

Onboarding source supports replacing with a URL and expects the zip file downloaded from the Microsoft Defender Security Center device management onboarding website. This role expects you'll host that file internally on an artifact server or as an unauthenticated LFS git object. If you keep the default `onboarding_source` value, it will deposit an empty json configuration file.
Also, if you manage your Tenant permissions by Group Tag you should set it at `mde_group_tag`. See [Add tag or group ID to the configuration profile](https://learn.microsoft.com/en-us/microsoft-365/security/defender-endpoint/linux-preferences?view=o365-worldwide#add-tag-or-group-id-to-the-configuration-profile) for further information.

    channel: prod
    onboarding_source: "{{ role_path }}/files/WindowsDefenderATPOnboardingPackage.zip"
    uninstall: false
    mde_pua_setting:
    mde_group_tag: YOUSHOULDSETIT

From the Microsoft documentation:

> Defender for Endpoint for Linux can be deployed from one of the following channels (denoted below as [channel]): insiders-fast, insiders-slow, or prod. Each of these channels corresponds to a Linux software repository.
>
> The choice of the channel determines the type and frequency of updates that are offered to your device. Devices in insiders-fast are the first ones to receive updates and new features, followed later by insiders-slow and lastly by prod.


You can set `mde_pua_setting` to "off", "audit" or "block" as described in the [Microsoft documentation](https://learn.microsoft.com/de-de/microsoft-365/security/defender-endpoint/linux-pua?view=o365-worldwide)

Dependencies
------------

The Role makes sure dependencies are met.

* curl
* unzip
* apt-transport-https (debian)
* gnupg (debian)
* python-apt (debian)

Example Playbook
----------------

This example presumes you have previously downloaded the individual onboarding package to your deploying machine. FOr example in your playbooks/file or ./file directory. Keep in Mind the onboarding package contains sensitive data you shoud not leak publicly!

    ---

    - name: Install Microsoft Defender Endpoint for Linux.
      hosts: all:!platform_windows
      become: true

      vars:
        onboarding_source: 'files/WindowsDefenderATPOnboardingPackage_Linux_Mgmt_Tool.zip'
        mde_group_tag: 'My_group_tag'
        mde_pua_setting: 'block'

      roles:
        - osp.mdelinux

License
-------

MIT

Author Information
------------------

This Role is initially provided by Felix Herzog (Otto Group Solution Provider GmbH)
